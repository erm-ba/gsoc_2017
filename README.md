# Google Summer of Code 2017
The development team of the [C3G](http://computationalgenomics.ca/) Montreal node is aiming to participate as an organization in Google Summer of Code 2017. We are offering to mentor students who wish to spend their summer working on interesting and user-friendly software pipelines for the analysis of Next-Generation Sequencing data. This page hosts information for prospective GSoC students and our project ideas. We are looking for 2 to 4 excellent students to collaborate on some of our research and development projects.

[**See GSoC 2017 full time line**](timeline.md)

--------------
Table of content:

[TOC]

--------------
# The CG3 montreal node 
The Montreal C3G nodes is hosted at the McGill University and Genome Quebec Innovation Center (MUGQIC). The Montreal node is strongly involved in the [GenAP](https://genap.ca/) developpement team and had developped a robust genomic data analysis pipeline set. Since 2011, we have completed more than 400 bioinformatics analysis projects with over 290 distinct groups of researchers across Canada. Our teams have significant experience in personalized medicine applications. These have included genome analysis and interpretation of personal genomes, technology and services to record patient presentations, DNA-, RNA- and ChIP-seq data analysis, and analysis of complete human epigenomes in both germline disorders and cancers.

The current members of the team are:

 * Guillaume Bourque, _Ph.D_ - Director
 * Mathieu Bourgey, _Ph.D_ - R&D Bioinformatics manager
 * Francois Lefebvre, _MSc_ - Services Bioinformatics manager
 * Edouard Henrion, _MSc_ - Software developer
 * David Bujold, _MSc_ - GenAP project manager
 * David Anderson, _MSc_ - Web designer
 * Robert Eveleigh, _MSc_ - Bioinformatics specialist
 * Gary Leveque, _MSc_ - Bioinformatics specialist
 * Emmanuel Gonzalez, _Ph.D_ - Bioinformatics consultant
 * Eloi Mercier, _MSc_ - Bioinformatics consultant
 * Pascale Marquis, _MSc_ - Bioinformatics specialist
 * Toby Hocking, _Ph.D_ - Postdoctoral researcher
 * Maiko Narahara, _Ph.D_ - Postdoctoral researcher
 * Jean Monlong, _MSc_ - PhD student
 * Patricia Goerner-Potvin, _MSc_ - PhD student
 * Maxime Caron, _MSc_ - PhD student
 * David Venuto, Master student
 * Joe Su, Master Student
 * Nestor Nebesio, Master Student

Collaborators:

 * Simon Gravel, _Ph.D_ - Assistant Professor at McGill University - [Simon's lab](simongravlabel.lab.mcgill.ca/Home.html)

# Mailing list

see information on the [mailing list page](mailing_list.md)

# Student application

Selected students will be automatically added to the mailing list

Selected student should follow the [student application guideline](Student_application.md)

# Proposed Projects 

## Supervised ChIP-seq pipeline

[MUGQIC pipelines](https://bitbucket.org/mugqic/mugqic_pipelines) consist of Python scripts which create a list of jobs running Bash commands. Those scripts support dependencies between jobs and smart restart mechanism if some jobs fail during pipeline execution.

[PeakSeg](http://jmlr.org/proceedings/papers/v37/hocking15.html) is a supervised machine learning model for ChIP-seq data analysis. A [preliminary set of R scripts for genome-wide supervised ChIP-seq peak calling](https://github.com/tdhock/PeakSegFPOP) has been developed, but it does not yet support dependencies between jobs, nor a smart restart mechanism.

The goal of this project is to develop a MUGQIC pipeline for supervised ChIP-seq peak calling, so that genomic researchers can more easily use this new machine learning method. Since coding the pipeline should not take more than 2-3 weeks, the rest of the GSOC project should be devoted to writing tests, documentation, and perhaps a blog.

### Developer profile

Required skills: Python, git.

Nice to have: R, C++.

### Selection tests

Follow the instructions on [the PeakSegFPOP page](https://github.com/tdhock/PeakSegFPOP#installation), install the software to your local computer. Run `test_demo.R` -- are the results that you observe on your computer the same as we computed?

Study the [Pull Request that Toby has created](https://bitbucket.org/mugqic/mugqic_pipelines/pull-requests/18/supervised-chipseq/diff). What steps are missing in this PR, relative to the current PeakSegFPOP GitHub repo?

What kinds of tests would you propose to make sure that the pipeline is working as intended?

### Test results

Students, post a new GitHub or Bitbucket repo with a README file that describes your test results, and add the link to your repo below. Then send an email to mentors.

### Mentors
[Toby Dylan Hocking](toby.hocking@mail.mcgill.ca), [Mathieu Bourgey](<mathieu.bourgey@computationalgenomics.ca>), [Edouard Henrion](<edouard.henrion@computationalgenomics.ca>)


## Improvements to SegAnnDB (interactive machine learning for DNA copy number data)

### Rationale

[SegAnnDB](https://github.com/tdhock/SegAnnDB) is a web app for interactive genomic segmentation of DNA copy number profiles, [published in Bioinformatics](http://bioinformatics.oxfordjournals.org/content/early/2014/02/03/bioinformatics.btu072.short). It combines previous work on data visualization, computer vision, and machine learning into a web site that uses annotated region labels to build a user-specific segmentation model for detection copy number alterations. YouTube videos explain how it works: [basic labeling](https://www.youtube.com/watch?v=BuB5RNASHjU), [labeling and exporting high-density profiles](https://www.youtube.com/watch?v=al0kk1JWsr0). However there are several limitations to the current code (1) there is no public SegAnnDB server, (2) there are almost no unit tests, and (3) it is not easy to use with [Galaxy](https://usegalaxy.org), and (4) the learning algorithm is too simple. This GSOC project aims to fix those issues.

### Coding project ideas

The main goal of this project is to create a public SegAnnDB instance on [GenAP](https://genap.ca/public/home). We will get a virtual machine on GenAP where you can log in as root and set up the SegAnnDB software. Another goal will be uploading a bunch of public data sets to this SegAnnDB instance.

- http://members.cbio.mines-paristech.fr/~thocking/data/aichi-backup.tgz
- http://members.cbio.mines-paristech.fr/~thocking/data/medulloblastoma/Nimblegen.tgz
- http://members.cbio.mines-paristech.fr/~thocking/data/medulloblastoma/snp6.tgz
- http://members.cbio.mines-paristech.fr/~thocking/data/neuroblastoma/2013/regions.csv

Another main goal will be to add unit tests for existing SegAnnDB functions, using [Pyramid recommendations](http://docs.pylonsproject.org/en/latest/community/testing.html). Ideally these tests will use a [headless browser](https://en.wikipedia.org/wiki/Headless_browser) to simulate someone using the web app (thus the Python and the JavaScript code will be tested). For example,

* when a profile is processed (plotter.db.Profile.process), test for creation of models and breakpoints in the database.
* when a profile is deleted, check for deletion of the profile and all related database objects and files (PNG scatterplots, probes.bedGraph.gz data).
* use a headless browser to create a 1breakpoint label, and then after the displayed model is updated, test to make sure there is a breakpoint rendered inside that label.

Another goal of this project is to integrate SegAnnDB with Galaxy. The idea is that users may already have their copy number data sets in Galaxy, and they may want to send them to SegAnnDB for analysis. And then after labeling and analysis on SegAnnDB, there should be an easy way for users to get their labels and models back into Galaxy. The student will need to develop a REST API so Galaxy can talk to SegAnnDB.

A final goal of this project would be for a student who is interested in machine learning. The current machine learning model implemented in [gradient_descent.py](https://github.com/tdhock/SegAnnDB/blob/master/plotter/gradient_descent.py) is un-regularized [max-margin interval regression](http://jmlr.org/proceedings/papers/v28/hocking13.html) using the square loss and two features. We could improve the accuracy by instead using L1-regularization with a larger set of features. The coding project would be to port the FISTA solver [from R code](https://github.com/tdhock/penaltyLearning/blob/master/R/IntervalRegression.R) to python.

### Developer profile

The ideal student is already familiar with web development, headless browser testing, and using Galaxy.

Required skills: git, JavaScript, Python. 

Nice to have: experience with [D3](http://d3js.org/) and [Pyramid](http://www.pylonsproject.org/projects/pyramid/about) web framework.

### Test questions

Please do not contact the mentors unless you have completed at least one of the tests below. Students who complete more tests, and more difficult tests, will be preferred.

Easy: Install SegAnnDB on your local machine by following the instructions in [INSTALL.sh](https://github.com/tdhock/SegAnnDB/blob/master/INSTALL.sh) or [Abhishek's blog post](http://abstatic.github.io/docker-segann.html). Choose one of the data sets (tgz links) above and upload all of those data sets onto your local SegAnnDB server, using [upload_profiles.py](https://github.com/tdhock/SegAnnDB/blob/master/plotter/static/upload_profiles.py). Make a YouTube video screencast where you explain how to label some breakpoints and copy number alterations in those data sets.

Medium: Set up a headless browser testing framework on your own computer, and make a screencast that shows you running [Abhishek's test cases](http://abstatic.github.io/selenium-for-testing.html).

Hard: Fork SegAnnDB and set up a code coverage utility ([CodeCov](https://codecov.io/), [Coveralls](https://coveralls.io/), etc) so we can monitor Python code coverage. SegAnnDB also includes C and JavaScript code. Can you figure out a way to get code coverage reports for these other code files as well?

### Test results

Students, post a new GitHub or Bitbucket repo with a README file that describes your test results, and add the link to your repo below. Then send an email to mentors.

### Mentors

- Toby Hocking <toby.hocking@mail.mcgill.ca> is the original author of SegAnnDB and will be the main mentor of this project (weekly skype calls and email support).
- David Morais <david.morais@gmail.com> is a secondary mentor who can help with Galaxy (email support).

## Flowchart creator for MUGQIC Pipelines

[MUGQIC pipelines](https://bitbucket.org/mugqic/mugqic_pipelines) consist of Python scripts which create a list of jobs running Bash commands. Those scripts support dependencies between jobs and smart restart mechanism if some jobs fail during pipeline execution. Job commands and parameters can be modified through several configuration files. 

We actually maintain 7 different pipelines and currently develop 3 others. Each pipeline contains between 10 to 40 steps. The development of these pipelines are in constant evolution and moreover users can decide to only run a selection of steps from the pipeline. Thus having an integrated system that automatically builds the  flowchart of the steps will be a user-friendly add-on to our software. 

The goal of this project is to develop the flowchart creation engine in the pipeline python object of our software.

### Developer profile

Required skills: Python, git.

Nice to have: Experience in flowchart development.

### Selection tests
Implement a test flochart in python for 5 first step of the [DNAseq pipeline](https://bitbucket.org/mugqic/mugqic_pipelines/src/master/pipelines/dnaseq/dnaseq.py).

the corresponding step are:  
 
 1. picard_sam_to_fastq       => generate fastq files from bam files
 2. trimmomatic               => clean fastq files
 3. merge_trimmomatic_stats   => merge individuals cleaning metrics
 4. bwa_mem_picard_sort_sam   => generate bam files from fastq files
 5. picard_merge_sam_files    => merge individuals bam files

Input data could be bam (incorporated at step 1 or 5) or fastq (incorporated at step 2).

Post the result of you implementation (code and folwchart) in an external repository and send us the link to it

### Mentors
[Mathieu Bourgey](<mathieu.bourgey@computationalgenomics.ca>) / [Edouard Henrion](<edouard.henrion@computationalgenomics.ca>)


-------------------------------

## Implement base modification analysis in the pacbio_assembly pipeline from MUGQIC Pipelines
[MUGQIC pipelines](https://bitbucket.org/mugqic/mugqic_pipelines) consist of Python scripts which create a list of jobs running Bash commands. Those scripts support dependencies between jobs and smart restart mechanism if some jobs fail during pipeline execution. 

Among the different pipelines we maintain, one of them is decidated to automate the generation of genomic assembly when sufficient coverage (>= 100x) of PacBio genomic data is available for a given organism. In summary, contigs assembly with PacBio reads is done using what is refer as the [HGAP workflow](https://github.com/PacificBiosciences/Bioinformatics-Training/wiki/HGAP-2.0). Briefly, raw subreads generated from raw .ba(s|x).h5 PacBio data files are filtered for quality. A subread length cutoff value is extracted from subreads, depending on subreads distribution, and used into the preassembly (aka correcting step) ([BLASR](https://github.com/PacificBiosciences/blasr)) step which consists of aligning short subreads on long subreads. Since errors in PacBio reads is random, the alignment of multiple short reads on longer reads allows to correct sequencing error on long reads. These long corrected reads are then used as seeds into assembly ([Celera assembler](https://sourceforge.net/projects/wgs-assembler/)) which gives contigs. These contigs are then polished by aligning raw reads on contigs (BLASR) that are then processed through a variant calling algorithm ([Quiver](https://github.com/PacificBiosciences/GenomicConsensus/blob/master/doc/HowToQuiver.rst)) that generates high quality consensus sequences using local realignments and PacBio quality scores. 

Once the assembly is generated we can use this sequence to reprocess the raw data in order to estimate the base modification pattern of the sample ([see Article](https://nar.oxfordjournals.org/content/early/2011/12/07/nar.gkr1146.full). To do this the PacBio developement team released the tool [MotifMaker](https://github.com/PacificBiosciences/MotifMaker). The goal of this project is to integrate the base modification analysis (using MotifMaker) in our pipeline while following our developpement standard.


### Developer profile

Required skills: Python, git.

Nice to have: Experience in Genomics and Pacbio data.

### Selection tests
Implement a fake step in the pacbio pipeline. This step should do the follow actions: 

 1 take the path of a reference sequence in the ini file
 2 Blat the largest polished contig against the reference sequence from step 1. 

Do not implement the blat function but use the one provided in the library [bfx/blat.py](https://bitbucket.org/mugqic/mugqic_pipelines/src/master/bfx/blat.py)

Post the result of you implementation in an external repository and send us the link to it
 

### Mentors
[Mathieu Bourgey](<mathieu.bourgey@computationalgenomics.ca>) / [Edouard Henrion](<edouard.henrion@computationalgenomics.ca>)

[Gary Leveque](<gary.leveque@computationalgenomics.ca>)

------------------------------------


## Develop add-ons for SCoNEs
[SCoNEs](https://bitbucket.org/mugqic/scones) is a development tools in R language which aims to call Copy Number Variation in paired cancer data (whole genome sequeuncing) using a Read Depth (RD) approach. The specificity of SCoNEs is to incorporate the individual (biological) variations of RD signal in order to adjust the set of paramter of the calling algorithm. The actual SCoNEs project is still in developpement and several add-ons are still to be implemented. 

The goal of the project is  to integrate two majors developpements:
   
    1 To implement a visualization engine (interactive genomic viewer) which could be use to manually explore the results
    2 To write the software documentation.

Addtionaly other add-on sugestions and developements from the student will be welcome.

### Developer profile

Required skills: R, git.

Nice to have: Experience in Cancer Genomics and statistics.

### Selection tests
Implement a basic viewer function in R for logRatio signal of the 2 samples contained in the [test data](data_test/SCONES_test.tsv) file.
Please motivated the choice of your method and implementation by adding comments in your code. 

Post the result of you implementation in an external repository and send us the link to it.

### Mentors
[Mathieu Bourgey](<mathieu.bourgey@computationalgenomics.ca>)

Jean Monlong