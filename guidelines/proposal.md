# Proposal guidelines (draft)

Applies to the following projects:

* EGA Data Submission database (mEGAdata) 
* Development of an HTML dynamic matrix to represent datasets with multiple dimensions

### General guidelines
Be sure to follow the general guidelines available in the [GSoC student guide](http://write.flossmanuals.net/gsocstudentguide/writing-a-proposal/).

### Format
The proposal should be submitted as a Markdown document, that you will make available from your public source control repository.

It should include the following sections, in this particular order:

* Name and Contact Information
* Project Title
* Synopsis (max 150 words)
* Overall strategy (max 500 words)
* Benefits to Community (max 250 words)
* Deliverables
* Biographical Information


### Additional notes
* Overall strategy section should give us the feeling that you have a pretty good idea of how you will solve the proposed problems.
* We welcome originality! If you have improvement ideas that go beyong the proposed project scope, by all means include them in your proposal.
